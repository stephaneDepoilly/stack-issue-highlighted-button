//
//  CustomWideButton.swift
//  CustomButtonView
//
//  Created by Stéphane DEPOILLY on 26/08/2017.
//  Copyright © 2017 Stephane.depoilly. All rights reserved.
//

import Foundation
import UIKit


enum WideButtonStyle {
	case standard
	case gold
	case facebook
	case google
}

class CustomWideButton: UIButton {
	
	
	// MARK: - PROPERTIES
	
	/** Colors declaration */
	let goldLight = UIColor(red:0.77, green:0.71, blue:0.51, alpha:1.0)
	let goldStandard = UIColor(red:0.72, green:0.65, blue:0.40, alpha:1.0)
	let goldDark = UIColor(red:0.68, green:0.59, blue:0.31, alpha:1.0)
	let blueGrayLight = UIColor(red:0.22, green:0.22, blue:0.26, alpha:1.0)
	let blueGrayStandard = UIColor(red:0.14, green:0.14, blue:0.16, alpha:1.0)
	let blueGrayDark = UIColor(red:0.07, green:0.07, blue:0.09, alpha:1.0)
	let blueFacebookStandard = UIColor(red:0.24, green:0.38, blue:0.60, alpha:1.0)
	let blueFacebookLight = UIColor(red:0.28, green:0.44, blue:0.69, alpha:1.0)
	let blueFacebookDark = UIColor(red:0.20, green:0.32, blue:0.51, alpha:1.0)
	let grayLight = UIColor(red:0.96, green:0.96, blue:0.96, alpha:1.0)
	let grayDark = UIColor(red:0.29, green:0.29, blue:0.29, alpha:1.0)
	
	/** Image names declaration*/
	let facebookLogo = "facebook-logo"
	let googleLogo = "google-logo"
	
	/** UI objects */
	private let imgvwLeftImage = UIImageView()
	private let lblTitle = UILabel()
	private let stackvwElements = UIStackView()
	
	/** Dynamic properties */
	var style: WideButtonStyle = .standard {
		didSet {
			setupUI()
		}
	}
	var isSelectable = true
	var title: (normal: String, highlighted: String , selected: String) = ("normal", "highlighted", "selected") {
		didSet {
			/** This is to avoid having selected or highlighted button without text */
			title.highlighted = title.highlighted != "" ? title.highlighted : title.normal
			title.selected = title.selected != "" ? title.selected : title.normal
			lblTitle.text = (isSelected && isSelectable && title.selected != "") ? title.selected.uppercased() : title.normal.uppercased()
		}
	}
	var leftImageName: String? = ""
	var normalBackgroundColor: UIColor? = UIColor(red:0.22, green:0.22, blue:0.26, alpha:1.0)
	var normalTextColor: UIColor? = UIColor(red:0.77, green:0.71, blue:0.51, alpha:1.0)
	var normalTextFont: UIFont? = UIFont(name: "HelveticaNeue-Medium", size: 12)
	
	var highlightedBackgroundColor: UIColor?
	var highlightedTextColor: UIColor? = .white
	var highlightedTextFont: UIFont? = UIFont(name: "HelveticaNeue-Medium", size: 12)
	
	var selectedBackgroundColor: UIColor? = UIColor(red:0.72, green:0.65, blue:0.40, alpha:1.0)
	var selectedTextColor: UIColor? = .white
	var selectedTextFont: UIFont? = UIFont(name: "HelveticaNeue-Medium", size: 12)
	
	var borderWidth: CGFloat? = 2
	var normalBorderColor: CGColor? = UIColor.gray.cgColor
	var cornerRadius: CGFloat? = 4
	
	/** Callback */
	var didTapButton: ((_ button: UIButton) -> ())?
	
	
	// MARK: - INITIAlIZATION
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setupUI()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		setupUI()
	}
	
	// MARK: - UIButton Methods
	
	override var isHighlighted: Bool {
		didSet {
			NSLog("isHighlighted on \(title) set to \(isHighlighted)")
			animateHighlight(isHighlightOn: isHighlighted)
		}
	}
	
	override var isSelected: Bool {
		didSet {
			animateSelection()
		}
	}
	
	
	// MARK: - METHODS
	
	private func setupUI() {
		
		/** Set up properties based on style */
		switch style {
		case .standard:
			normalBackgroundColor = blueGrayLight
			highlightedBackgroundColor = goldStandard
			selectedBackgroundColor = goldStandard
			borderWidth = 2
			normalBorderColor = blueGrayStandard.cgColor
			cornerRadius = 4
		case .gold:
			normalBackgroundColor = goldLight
			highlightedBackgroundColor = goldStandard
			selectedBackgroundColor = goldStandard
			borderWidth = 2
			normalBorderColor = goldStandard.cgColor
			cornerRadius = 4
		case .facebook:
			leftImageName = facebookLogo
			normalBackgroundColor = blueFacebookStandard
			normalTextColor = .white
			highlightedBackgroundColor = blueFacebookDark
			highlightedTextColor = .white
			selectedBackgroundColor = blueFacebookStandard
			borderWidth = 2
			normalBorderColor = blueFacebookDark.cgColor
			cornerRadius = 4
		case .google:
			leftImageName = googleLogo
			normalBackgroundColor = .white
			normalTextColor = grayDark
			highlightedBackgroundColor = grayLight
			highlightedTextColor = grayDark
			selectedBackgroundColor = grayLight
			borderWidth = 2
			normalBorderColor = grayLight.cgColor
			cornerRadius = 4
		}
		
		/** Set up button behaviour */
		self.isUserInteractionEnabled = true
		self.addTarget(self, action: #selector(buttonIsTapped), for: .touchUpInside)
		
		
		/** Set up button style */
		self.backgroundColor = (isSelected && isSelectable) ? selectedBackgroundColor : normalBackgroundColor
		self.layer.borderWidth = borderWidth!
		self.layer.borderColor = normalBorderColor
		self.layer.cornerRadius = cornerRadius!
		self.layer.masksToBounds = true
		
		/** Set up label */
		lblTitle.text = (isSelected && isSelectable) ? title.selected.uppercased() : title.normal.uppercased()
		lblTitle.font = (isSelected && isSelectable) ? selectedTextFont : normalTextFont
		lblTitle.textColor = (isSelected && isSelectable) ? selectedTextColor : normalTextColor
		lblTitle.textAlignment = leftImageName != "" ? .left : .center
		
		/** Set up image view */
		imgvwLeftImage.isHidden = leftImageName != "" ? false : true
		imgvwLeftImage.contentMode = .scaleAspectFit
		imgvwLeftImage.image = leftImageName != "" ? UIImage(named: leftImageName!) : nil
		
		/** Set up stack view */
		stackvwElements.isUserInteractionEnabled = false
		stackvwElements.addArrangedSubview(imgvwLeftImage)
		stackvwElements.addArrangedSubview(lblTitle)
		stackvwElements.axis = .horizontal
		stackvwElements.alignment = .center
		stackvwElements.distribution = .fill
		stackvwElements.spacing = 8
		addSubview(stackvwElements)
		
		/** Set up auto-layout */
		stackvwElements.translatesAutoresizingMaskIntoConstraints = false
		stackvwElements.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
		stackvwElements.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -8).isActive = true
		stackvwElements.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
		stackvwElements.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 8).isActive = true
		imgvwLeftImage.translatesAutoresizingMaskIntoConstraints = false
		imgvwLeftImage.setContentHuggingPriority(UILayoutPriorityRequired, for: .horizontal)
	}
	
	func buttonIsTapped(_ sender: UIButton) {
		sendActions(for: .valueChanged)
		/** Fixes the problem of the highlight only triggered when dragging your finger on the button*/
		isSelected = isSelectable ? !isSelected : false
		didTapButton?(sender)
	}
	
	private func animateHighlight(isHighlightOn : Bool) {
		NSLog("animateHighlight enter: triggered by isHighlighted on \(title) set to \(isHighlighted)")
			UIView.animate(withDuration: 0.3, animations: {
				self.backgroundColor = isHighlightOn ? self.highlightedBackgroundColor : self.normalBackgroundColor
				self.lblTitle.text = isHighlightOn ? self.title.highlighted : self.title.normal
				self.lblTitle.font = isHighlightOn ? self.highlightedTextFont : self.normalTextFont
				self.lblTitle.textColor = isHighlightOn ? self.highlightedTextColor : self.normalTextColor
			})
	}
	
	private func animateSelection() {
		backgroundColor = (isSelected && isSelectable) ? selectedBackgroundColor : normalBackgroundColor
		lblTitle.text = (isSelected && isSelectable && title.selected != "") ? title.selected.uppercased() : title.normal.uppercased()
		lblTitle.font = (isSelected && isSelectable) ? selectedTextFont : normalTextFont
		lblTitle.textColor = (isSelected && isSelectable) ? selectedTextColor : normalTextColor
	}
	
}
