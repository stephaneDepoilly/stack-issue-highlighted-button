//
//  ViewController.swift
//  StackO_HighlightFunctionCall
//
//  Created by Stéphane DEPOILLY on 28/08/2017.
//  Copyright © 2017 Stephane DEPOILLY. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	// MARK: - IBOUTLETS
	
	@IBOutlet weak var btnRegular: CustomWideButton! {
		didSet {
			btnRegular.style = .standard
			btnRegular.title = ("Log In".uppercased(), "Log In (highlighted)".uppercased(), "Log In (selected)".uppercased())
		}
	}
	@IBOutlet weak var btnFacebook: CustomWideButton! {
		didSet {
			btnFacebook.style = .facebook
			btnFacebook.title = ("Sign up with Facebook".uppercased(), "Sign up (highlighted)".uppercased(), "Sign up (selected)".uppercased())
		}
	}
	@IBOutlet weak var btnGoogle: CustomWideButton! {
		didSet {

		}
	}
	
	
	// MARK: - VIEW LIFE CYCLE
	
	override func viewDidLoad() {
		super.viewDidLoad()
		btnGoogle.style = .google
		btnGoogle.title = ("Sign up with Google".uppercased(), "Sign up (highlighted)".uppercased(), "Sign up (selected)".uppercased())
	}


}

